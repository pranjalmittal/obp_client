# -*- coding: utf-8 -*-

{
    'name': 'Open Bank Project Client',
    'version': '1.0',
    'category': 'Tools',
    'description': """
To make RESTful queries to the Open Bank Project API and retrieve data.
============================================================================
""",
    'author': 'pramttl',
    'maintainer': 'pramttl',
    'website': 'http://www.openbankproject.com',
    'depends': ['base', 'web', 'base_setup'],
    'data': [
        #'obp_client_data.xml',
        'obp_client_view.xml',
        #'security/ir.model.access.csv'
    ],
    #'js': ['static/src/js/obp_client.js'],
    'css': [
        'static/lib/zocial/css/zocial.css',
        'static/src/css/obp_client.css',
    ],
    #'qweb': ['static/src/xml/obp_client.xml'],
    'installable': True,
    'auto_install': False,
}
