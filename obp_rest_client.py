#--This is the location from where a client application gets user resources from.
API_LOCATION = 'https://demo.openbankproject.com/sandbox/obp/v1.1'
OAUTH_HEADER_FILE = 'saved_oauth_header.txt'

# These settings overwrite the defaults above.
from settings import *

import requests
import pickle
import sys
import simplejson as json
import openerplib
from pprint import pformat,pprint

DEFAULT = False

# Opening DEBUG_FILE
debug_file = open('debug.txt','w')

# Creating a connection to the OpenERP database.
connection = openerplib.get_connection(hostname=DB_HOST, database=DB_NAME,
                                       login=DB_USER, password=DB_PASSWORD)
bank_model = connection.get_model("obp.bank")

# Loading the OAuth Header object from the file where it was saved.
f = open(OAUTH_HEADER_FILE, 'r')
oauth = pickle.load(f)
f.close()


# Case 1: Fetching details of all the banks.
resource_url = API_LOCATION + '/banks'

debug_file.write("Fetching data from: ")
debug_file.write(resource_url)
r = requests.get(url=resource_url, auth=oauth)

#Converting the obtained content into a useful python object.
content = json.loads(r.content)
content_string = pformat(content)
debug_file.write('\n%s\n\n'%content_string)

# Loading each item into the OpenERP data Model.
bank_list = []
for item in content['banks']:
    record_id = bank_model.create({
               'full_name': item['bank']['full_name'],
               'logo' : item['bank']['logo'],
               'website': item['bank']['website'],
               'bank_id': item['bank']['id'],
               'short_name': item['bank']['short_name'],
                })

'''
# Case 2: Fetching details of all the accounts.
for item in content['accounts']:
    record_id = bank_model.create({
               'full_name': item['account']['full_name'],
               'logo' : item['bank']['logo'],
               'website': item['bank']['website'],
               'bank_id': item['bank']['id'],
               'short_name': item['bank']['short_name'],
                })
'''

debug_file.close()
