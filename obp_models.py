from openerp.osv import osv, fields

#--This is the location from where a client application gets user resources from.
API_LOCATION = 'https://demo.openbankproject.com/sandbox/obp/v1.1'
OAUTH_HEADER_FILE = 'saved_oauth_header.txt'

# These settings overwrite the defaults above.
from settings import *

import requests
import pickle
import sys
import simplejson as json

DEFAULT = False


class Bank(osv.Model):

    """This class defines a Bank Object"""

    _name = 'obp.bank'
    _description = 'OBP Bank'
    _order = 'bank_id'

    _columns = {
        'full_name' : fields.char('Bank name'),
        'logo' : fields.char('Bank Logo'),
        'website' : fields.char('Bank Website'),
        'bank_id' : fields.char('Bank ID'),
        'short_name' : fields.char('Short Name'),
    }


    def load_obp_data():
        resource_url = API_LOCATION + '/banks'
        df = open('debug.txt','w')
        df.write("Fetching data from the following endpoint:\n")
        df.write(resource_url)

        # Loading the OAuth Header object from the file where it was saved.
        f = open(OAUTH_HEADER_FILE, 'r')
        oauth = pickle.load(f)
        f.close()

        r = requests.get(url=resource_url, auth=oauth)

        #Converting the obtained content into a useful python object.
        content = json.loads(r.content)

        for item in content['banks']:
            record_id = self.create(cr, uid, 
                      {'full_name': item['bank']['full_name'],
                       'logo' : item['bank']['logo'],
                       'website': item['bank']['website'],
                       'bank_id': item['bank']['id'],
                       'short_name': item['bank']['short_name'],},
                        context=context)
        df.close()


Bank()

        
